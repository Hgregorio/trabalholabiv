package loja;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter

public class Produto {

	private int idProduto;
	private double precoVenda;
	private String descricao;
	private String categoria;

	public Produto(int idProduto, double precoVenda, String descricao, String categoria) {
		super();
		this.idProduto = idProduto;
		this.precoVenda = precoVenda;
		this.descricao = descricao;
		this.categoria = categoria;
	}

}
