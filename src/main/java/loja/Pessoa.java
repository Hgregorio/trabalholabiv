package loja;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter

public class Pessoa {
	
	private String nome;
	private String telefone;
	private String endereco;
	private String cpf;
	private String dataNascimento;
	
	
	public Pessoa(String nome, String telefone, String endereco, String cpf, String dataNascimento) {
		super();
		this.nome = nome;
		this.telefone = telefone;
		this.endereco = endereco;
		this.cpf = cpf;
		this.dataNascimento = dataNascimento;
	}

}
