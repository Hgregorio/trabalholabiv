package loja;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter

public class ItensVenda {
	
	private int idProduto;
	private int idVenda;
	private double quantidadeProduto;
	
	public ItensVenda(int idProduto, int idVenda, double quantidadeProduto) {
		super();
		this.idProduto = idProduto;
		this.idVenda = idVenda;
		this.quantidadeProduto = quantidadeProduto;
	}

}
