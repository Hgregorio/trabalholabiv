package loja;

import java.time.LocalDate;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter

public class Venda {
	
	private int idVenda;
	private LocalDate dataVenda;
	
	public Venda(int idVenda, LocalDate dataVenda) {
		super();
		this.idVenda = idVenda;
		this.dataVenda = dataVenda;
	}
	

}
