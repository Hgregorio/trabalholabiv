package loja;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter

public class Fornecedor {
	
	private int idFornecedor;
	private String endereco;
	private String telefone;
	private String descricao;
	private String cnpj;
	
	public Fornecedor(int idFornecedor, String endereco, String telefone, String descricao, String cnpj) {
		super();
		this.idFornecedor = idFornecedor;
		this.endereco = endereco;
		this.telefone = telefone;
		this.descricao = descricao;
		this.cnpj = cnpj;
	}

}
