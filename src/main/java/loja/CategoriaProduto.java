package loja;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter

public class CategoriaProduto {
	
	private int idCategoria;
	private String nome;
	
	public CategoriaProduto(int idCategoria, String nome) {
		super();
		this.idCategoria = idCategoria;
		this.nome = nome;
	}
	

}
