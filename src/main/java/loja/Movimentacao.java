package loja;

import java.time.LocalDate;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter

public class Movimentacao {
	
	private int idFornecedor;
	private double precoEntranda;
	private String descricaoProduto;
	private LocalDate dataVenda;
	
	public Movimentacao(int idFornecedor, double precoEntranda, String descricaoProduto, LocalDate dataVenda) {
		super();
		this.idFornecedor = idFornecedor;
		this.precoEntranda = precoEntranda;
		this.descricaoProduto = descricaoProduto;
		this.dataVenda = dataVenda;
	}

}
