package loja;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter

public class Estoque {
	
	private int quantidade;
	private double custoUnitario;
	
	public Estoque(int quantidade, double custoUnitario) {
		super();
		this.quantidade = quantidade;
		this.custoUnitario = custoUnitario;
	}

}
