package loja;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter

public class Funcionario extends Pessoa {
	
	private int idFuncionario;
	private String cargo;
	private String login;
	private String senha;
	
	public Funcionario(String nome, String telefone, String endereco, String cpf, String dataNascimento) {
		super(nome, telefone, endereco, cpf, dataNascimento);
		// TODO Auto-generated constructor stub
	}

}
