package loja;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter

public class FormaPagamento {
	
	private String tipo;
	private int numeroParcelas;
	
	public FormaPagamento(String tipo, int numeroParcelas) {
		super();
		this.tipo = tipo;
		this.numeroParcelas = numeroParcelas;
	}
	

}
